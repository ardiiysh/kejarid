class CreateStudents < ActiveRecord::Migration[6.0]
  def up
    create_table :students do |t|
      t.string :name ,default: 'nama', limit: 100
      t.string :username ,default: 'username', limit: 100
      t.integer :age ,default: 0
      t.string :rombel , default:'rombel'
      t.text :address ,default:'address' ,limit:100
      t.string :city ,default:'city' ,limit:100
      t.integer :nik 

      t.timestamps
    end
  end
  def down 
    drop_table :students
end
