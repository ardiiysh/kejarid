class CreateTeachers < ActiveRecord::Migration[6.0]
  def up
    create_table :teachers do |t|
      t.string :nik  ,default: 'nik', limit: 100
      t.string :nama , default: 'nama', limit: 100
      t.integer :age  , default: 0
      t.string :kelas  ,default: 'kelas', limit: 100
      t.string :mapel , default: 'kelas', limit: 100

      t.timestamps
    end
  end
  def down 
    drop_table :teachers
end
