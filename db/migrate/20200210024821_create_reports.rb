class CreateReports < ActiveRecord::Migration[6.0]
  def up
    create_table :reports do |t|
      t.string :title ,default:'title' , limit:100
      t.string :hasil ,default:0
      t.string :mapel ,default:''
      t.string :teacher_id ,default:''
      t.string :student_id ,default:''
      t.date :date 

      t.timestamps
    end
  end
  def down 
    drop_table :reports
end
