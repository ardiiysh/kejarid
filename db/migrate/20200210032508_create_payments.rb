class CreatePayments < ActiveRecord::Migration[6.0]
  def up
    create_table :payments do |t|
      t.string :id_transsction ,default:0
      t.string :status ,default:'status'
      t.string :upload 

      t.timestamps
    end
  end
  def down 
    drop_table :payments
end
