require 'test_helper'

class ExampsControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get examps_new_url
    assert_response :success
  end

  test "should get edit" do
    get examps_edit_url
    assert_response :success
  end

  test "should get index" do
    get examps_index_url
    assert_response :success
  end

  test "should get show" do
    get examps_show_url
    assert_response :success
  end

end
